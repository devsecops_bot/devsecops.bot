import requests, json, os, time, sys
from lxml.html import fromstring


class SonarQubeReportSlack:

    def __init__(self):
        self.threshold = os.getenv("FAIL_THRESHOLD",5)
        self.slack_token = os.getenv("SLACK_TOKEN")
        self.slack_channel = os.getenv("SLACK_CHANNEL")
        self.fail_build = os.getenv("AI_FAIL_BUILD", "false")
        self.component = os.getenv("SONAR_COMPONENT","sonar_proj")
        self.sonar_url = os.getenv("SONAR_URL","http://sonarqube:9000")
        self.sonar_username = os.getenv("SONAR_USERNAME","admin")
        self.sonar_password = os.getenv("SONAR_PASSWORD","admin")
        self.issues_dict = {}

    def wait_for_analysis(self):
        ATTEMPTS = 10
        url = self.sonar_url + "/api/ce/component?component=%s" % (self.component,)
        while True:
            res = requests.get(url, auth=(self.sonar_username, self.sonar_password)).json()
            if "queue" not in res.keys() or not res["queue"] or ATTEMPTS == 0:
                break
            time.sleep(10)
            ATTEMPTS -= 1

    def generate_summary_and_report(self):
        cmd = """sonar-report  --sonarurl="%s" --sonarusername="%s" --sonarpassword="%s"  --sonarcomponent="%s" """
        cmd = cmd % (self.sonar_url, self.sonar_username, self.sonar_password, self.component)
        os.system(cmd)
        count = 0
        print("executed the command")
        with open('report.html') as f: report = f.read()
        try:
            count, summary, summarytable = self.generate_summary(report)
            print("summarytable", summarytable)
            print("summary", summary)
        except Exception as e:
            print("Error in generating the summarytable, no problem",str(e) )

        if self.slack_token:
            slack_status = self.post_file_to_slack(
                "Report of SaST",
                'Report.html',
                report)
            print(slack_status)
        else:
            print("Slack is not configured yet")

        # Block Build in case of blocker
        if self.fail_build:
            if int(count) > self.threshold and self.fail_build == "true":
                print("Failing build as count of the critical issues are higher than threshold")
                sys.exit(1)
            else:
                print("Build is secure enough to be passed")


    def generate_summary(self, report):
        count = 0
        stable = ""
        summary_table = ""
        html_str = fromstring(report)
        print("HTML_String", html_str)
        issues = html_str.xpath("//div[@class='summup']//tr/td/text()")
        isitr = iter(issues)
        issues_dict = dict(zip(isitr, isitr))
        self.issues_dict = issues_dict
        print("This is the issue dict", issues_dict)
        summary_table = self.get_summary_table(issues_dict)
        count = int(issues_dict.get("BLOCKER", 0)) + int(issues_dict.get("CRITICAL", 0))
        return count, "SAST %s: %s Blocker/Critical Issues Identified in the Repository" % (
        self.component, str(count)), summary_table

    def post_file_to_slack(
            self, text, file_name, file_bytes, file_type=None, title='SonarQube Vulnerability Report '
    ):
        status = requests.post(
            'https://slack.com/api/files.upload',
            {
                'token': self.slack_token,
                'filename': file_name,
                'channels': self.slack_channel,
                'filetype': file_type,
                'initial_comment': text,
                'title': title
            },
            files={'file': file_bytes}).json()
        return status

    def get_summary_table(self, issues_dict):
        return "| Severity | Number of Issues |%0A| --- | --- |%0A| BLOCKER | {blocker} " \
               "  |%0A| CRITICAL | {critical}   |%0A| MAJOR | {major} " \
               " |%0A| MINOR | {minor}  |".format(blocker=issues_dict.get("BLOCKER", "0"),
                                                  critical=issues_dict.get("CRITICAL", "0"),
                                                  major=issues_dict.get("MAJOR", "0"),
                                                  minor=issues_dict.get("MINOR", "0"))
    def open_ai_can_fail(self):
        return False

    def run(self):
        self.wait_for_analysis()
        try:
            self.generate_summary_and_report()
            if self.fail_build and self.open_ai_can_fail():
                sys.exit("OpenAI decided to fail the build")
        except Exception as e:
            print(str(e),"Probably empty report, no issues")


SonarQubeReportSlack().run()
