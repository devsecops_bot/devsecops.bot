# Devsecops.Bot
## Usage

Use this component to perform a DAST scan a REST Api and get a report for vulnerabilities.

```yaml
include:
  - component: gitlab.com/devsecops_bot/devsecops.bot
    inputs:
      stage: scan
      
```


include:
  - component: gitlab.com/c2708/dast
    inputs:
      stage: test
      format: openapi
